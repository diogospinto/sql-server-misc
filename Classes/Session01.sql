---- a
--Create Database Projetos

--Go

--Use Projetos

---- b
--Create Table Departamento (
--	DepNum Int Identity Constraint pk_Departamento_DepNum Primary Key, 
--	Nome Varchar(100) Constraint nn_Departamento_Nome Not Null, 
--	Local Varchar(60) Constraint nn_Departamento_Local Not Null
--)

--Go

--Create Table Projeto (
--	ProjNum Int Identity Constraint pk_Projeto_ProjNum Primary Key, 
--	Designacao Varchar(100) Constraint nn_Projeto_Designacao Not Null, 
--	Fundos Numeric(5,2)
--)

--Go

--Create Table Empregado (
--	EmpNum Int Identity Constraint pk_Empregado_EmpNum Primary Key, 
--	Nome Varchar(100) Constraint nn_Empregado_Nome Not Null, 
--	Categoria Varchar(60),
--	Salario Numeric(5,2), 
--	DepNum Int Constraint fk_Empregado_DepNum Foreign Key References Departamento(DepNum)
--)

--Go

--Create Table Atribuicao (
--	ProjNum Int Constraint fk_Atribuicao_ProjNum Foreign Key References Projeto(ProjNum), 
--	EmpNum Int Constraint fk_Atribuicao_EmpNum Foreign Key References Empregado(EmpNum), 
--	Funcao Varchar(60), 
--	Constraint pk_Atribuicao_ProjNum_EmpNum Primary Key(ProjNum, EmpNum)
--)

Go

-- c
Insert Into Departamento(Nome, Local)
	Values ('Log�stica', 'Francelos'), 
		   ('Mec�nica', 'Gondomar'), 
		   ('Recursos Humanos', 'Porto'), 
		   ('Inova��o e Tecnologias', 'Maia'), 
		   ('Vendas', 'Porto')

Select *
From Departamento

-- d
Insert Into Empregado(Nome, Categoria, Salario, DepNum)
	Values ('Ant�nio Jorge Trindade', 'Engenheiro Mec�nico', 2000, 2), 
		   ('Ana Lu�sa Freitas Bastos', 'Gestora', 1650, 3), 
		   ('Filipe Lencastre Amaral', 'Soci�logo', 1650, 3), 
		   ('Andr� Maia da Luz', 'Diretor Comercial', 2000, 5), 
		   ('Francisco Vieira dos Santos', 'Gestor', 1250, 1)

Select *
From Empregado

-- e
Insert Into Projeto(Designacao, Fundos)
	Values ('+ Controlo Baterias El�tricas', 20000), 
		   ('ParticipaNaInovocaoMovel', 75000), 
		   ('4 Fusao Optica Rent�vel', 100000), 
		   ('CoLab Felicidade', 35000), 
		   ('Partilha Redes', 2000)

Select *
From Projeto

-- f
Insert Into Atribuicao(ProjNum, EmpNum, Funcao)
	Values (1, 1, 'Desenvolvimento'), 
		   (1, 2, 'Coordenador'), 
		   (1, 3, 'Motivador'), 
		   (2, 1, 'Desenvolvimento'), 
		   (4, 3, 'Coordenador'), 
		   (2, 2, 'Desenvolvimento'), 
		   (2, 5, 'An�lise de Requisitos')

Select *
From Atribuicao

-- g
Alter Table Empregado
Add DataAniversario Date

Alter Table Atribuicao
Add DataAtribuicao Date

Update Empregado
Set DataAniversario = '1970-01-01'

Update Atribuicao
Set DataAtribuicao = '1970-01-01'

Alter Table Empregado
Alter Column DataAniversario Date Not Null

Alter Table Atribuicao
Alter Column DataAtribuicao Date Not Null

Select *
From Atribuicao

-- h
Alter Table Atribuicao
Alter Column Funcao Varchar(60) Not Null

-- i
Alter Table Atribuicao
Add Constraint df_Atribuicao_DataAtribuicao Default GetDate() For DataAtribuicao

-- j
Select *
From Departamento

Update Departamento
Set Local = 'Matosinhos'
Where Nome = 'Log�stica'

-- k
Select *
From Projeto

Delete From Projeto
Where Projeto.ProjNum Not In (Select Atribuicao.ProjNum 
							  From Atribuicao)

-- l
Insert Into Empregado(Nome, Categoria, Salario, DepNum, DataAniversario)
	Values ('David Mateus', 'Motorista', 2000, 4, '1984-07-03')

Update Empregado
Set Salario = Salario + (Salario * 0.10)
Where Empregado.DepNum = (Select Dep.DepNum From Departamento Dep Where Dep.Nome = 'Inova��o e Tecnologias')

-- m
-- i
Select *
From Empregado

-- ii
Select Empregado.EmpNum, Empregado.Nome
From Empregado

-- iii
Select *
From Projeto
Order by Projeto.Designacao Desc

-- iv
Select *
From Empregado
Where Categoria Like 'Gestor%'

-- v
Select *
From Departamento
Where Local In('Matosinhos', 'Gondomar')

-- vi
Insert Into Departamento(Nome, Local)
	Values ('Marketing', 'Matosinhos')

Select Distinct Departamento.Nome, Departamento.DepNum
From Departamento
Where Departamento.DepNum In (Select Empregado.DepNum
							  From Empregado)
Order by Departamento.Nome Asc

-- vii
Select *
From Empregado
Where Month(Empregado.DataAniversario) < 4

-- viii
Select *
From Empregado
Where Empregado.Salario Between 1500 And 1950

-- ix
Select Count(Empregado.EmpNum)
From Empregado
Where Empregado.DepNum In (Select Departamento.DepNum From Departamento Where Local In('Porto', 'Maia'))

-- x
Select Avg(Empregado.Salario) as 'Avg Salary Sales'
From Empregado
Where Empregado.DepNum In (Select Departamento.DepNum From Departamento Where Departamento.Nome = 'Vendas')

-- xi
Select Count(Empregado.EmpNum) as 'Employees per Project', Projeto.ProjNum
From Empregado, Projeto, Atribuicao
Where Empregado.EmpNum = Atribuicao.EmpNum And Projeto.ProjNum = Atribuicao.ProjNum
Group By Projeto.ProjNum

Select Count(Atribuicao.ProjNum), Designacao
From Atribuicao, Projeto
Where Atribuicao.ProjNum = Projeto.ProjNum
Group by Designacao

Select *
From Atribuicao

-- xii
Select Empregado.Nome
From Empregado
Where Empregado.Salario = (Select Min(Empregado.Salario) From Empregado)