-- Parte 1

-- 1
Create Database Modelo

Go

Use Modelo

-- 2
--Create Table Seccao(
--	NumSec Int Identity(10, 10) Constraint pk_Seccao_NumSec Primary Key,
--	Nome Varchar(30) Not Null,
--	Cidade Varchar(40) Not Null
--)

--Create Table Empregado(
--	NumEmp Int Identity Constraint pk_Empregado_NumEmp Primary Key,
--	Nome Varchar(50) Not Null,
--	Seccao Int Not Null Constraint fk_Empregado_Seccao Foreign Key References Seccao(NumSec),
--	Posto Varchar(50) Not Null, 
--	Chefe Int Constraint fk_Empregado_Chefe Foreign Key References Empregado(NumEmp),
--	Salario Numeric(4) Not Null,
--	Comissao Int Not Null
--)

-- 3
Insert Into Seccao(Nome, Cidade)
	Values ('Fabrico', 'Porto'), 
		   ('Comercial', 'Porto'), 
		   ('Marketing', 'Braga'), 
		   ('Planeamento', 'Guimar�es'), 
		   ('Administra��o', 'Porto'), 
		   ('Inform�tica', 'Braga'), 
		   ('Recursos Humanos', 'Guimar�es')

Select *
From Seccao

Insert Into Empregado(Nome, Seccao, Posto, Chefe, Salario, Comissao)
	Values ('Ana', 10, 'Programador', 3, 3000, 10), 
		   ('Nuno', 70, 'Engenheiro', 1, 1500, 40), 
		   ('�lvaro', 50, 'Administrador', Null, 2500, 0), 
		   ('Ant�nio', 10, 'Engenheiro', 3, 1450, 20), 
		   ('Susana', 20, 'Administrador', Null, 2750, 30), 
		   ('Cl�udio', 60, 'Vendedor', 4, 1000, 50)

Select *
From Empregado

-- Parte 2

-- 1
Select Seccao.Nome, Seccao.Cidade
From Seccao
Where Seccao.NumSec = 70

-- 2
Select Empregado.Posto, Empregado.Salario, Empregado.Comissao, Salario * Comissao as 'Sal�rio c/ Comiss�o'
From Empregado
Where Salario < (Comissao * Salario / 100)

-- 3
Select Seccao.Nome
From Seccao
Where Cidade = 'Porto'

-- 4
Select Distinct Empregado.Posto
From Empregado

-- 5
Select Empregado.Nome, Empregado.Posto, Empregado.Seccao
From Empregado
Where Empregado.Seccao In(20, 30, 40)

-- 6
Select Sum(Salario * 12 + (Comissao * Salario / 100 * 12))
From Empregado
Where Posto In('Programador', 'Engenheiro')

-- 7
Select NumEmp, Nome, Posto
From Empregado
Order by NumEmp

-- 8
Select Seccao, Count(*) as 'Total Empregados', Sum(Empregado.Salario * Empregado.Comissao + Empregado.Salario) as 'Total de sal�rios'
From Empregado
Group by Seccao

-- 9
Select Empregado.Seccao, Empregado.Posto, Count(*) as 'Total Empregados', Avg(Empregado.Salario) as 'M�dia Sal�rio'
From Empregado
Where Seccao In(10, 20, 30)
Group by Seccao, Posto

-- 10
Select Sum(Salario * 12 + (Comissao * Salario / 100 * 12)) as 'Gasto Anual Vendedores', Seccao, Count(*) as 'Total Vendedores'
From Empregado
Where Posto = 'Vendedor'
Group by Seccao

-- 11
Select Seccao, (Count(Empregado.NumEmp) * 100.0 / (Select Count(*) From Empregado)) as 'Percentagem'
From Empregado
Group by Seccao

-- 12
Select Avg(Salario) as 'Sal�rio M�dio'
From Empregado
Where Posto <> 'Administrador'

-- 13
Select Nome, Posto
From Empregado
Where Comissao * Salario / 100 > (Select Max(Comissao * Salario / 100) From Empregado Emp Where Empregado.NumEmp <> Emp.NumEmp)

-- 14
Select Nome, Posto, Salario
From Empregado
Where Seccao Not In(10) And Salario In(Select Salario 
									   From Empregado 
									   Where Seccao = 10) And Posto In(Select Posto 
																	   From Empregado
																	   Where Seccao = 10)

-- 15
Select Top(1) Seccao, Nome, Posto, Comissao * Salario / 100 as 'Sal�rio Total'
From Empregado
Where Comissao * Salario / 100 < (Select Max(Comissao * Salario / 100) From Empregado)
Order by Comissao * Salario / 100 Desc

-- 15 - Com Rank
With CTE as 
	(Select Seccao, Nome, Posto, Salario * (1 + Comissao * 1.0 / 100) as SalarioT,
	 Rank() Over (Order by Salario * (1 + Comissao / 100) Desc) as Ordenacao
	 From Empregado)
Select c.Seccao, c.Nome, c.Posto, c.SalarioT
From CTE c
Where c.Ordenacao = 2

-- 16
Select Seccao, Nome, Posto, Salario * (1 + Comissao * 1.0 / 100) as 'Sal�rio Total', Comissao
From Empregado
Where Posto = 'Vendedor' And Seccao = 20 And Salario * (1 * Comissao * 1.0 / 100) = (Select Max(Salario * (1 * Comissao * 1.0 / 100)) From Empregado Where Posto = 'Vendedor')
Order by Salario * (1 + Comissao * 1.0 / 100) Desc

-- 17
Select Seccao, Avg(Salario) as 'Sal�rio M�dio'
From Empregado
Group by Seccao
Having Avg(Salario) > 2000

-- 18
Select Seccao, Count(NumEmp) as 'Nr de Vendedores'
From Empregado
Where Posto = 'Vendedor'
Group by Seccao
Having Count(NumEmp) > 2

-- 19
Select Seccao, Avg(Salario) as 'Sal�rio M�dio'
From Empregado
Group by Seccao
Having Avg(Salario) > (Select Avg(Salario) From Empregado)

-- 20
Select Nome, Posto, Salario
From Empregado
Where Seccao In(Select Seccao
				From Empregado
				Group by Seccao
				Having Avg(Salario) > (Select Avg(Salario) From Empregado))

-- 21
Select Empregado.*, Seccao.*
From Empregado, Seccao
Where Empregado.Seccao = Seccao.NumSec