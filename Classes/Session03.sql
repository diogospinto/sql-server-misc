-- 1
-- a
--Create Table utilizadores(
--	id Int Not Null Identity(1, 1),
--	nome Varchar(500) Not Null,
--	email Varchar(100) Not Null,
--	Primary Key(id)
--)

--Create Table pontos(
--	id Int Not Null Identity(1, 1),
--	user_id Int Not Null,
--	pontos Int Default '0',
--	Primary Key(id),
--	Foreign Key(user_id) References utilizadores(id)
--)

-- b
Create Trigger trg_adicionar_pontos 
On utilizadores 
After Insert 
As
Begin
	Declare @userID int
	Select @userID = i.id From inserted i
	Insert Into pontos(user_id)
		Values(@userID)
End

Select *
From utilizadores
Select *
From pontos

Insert Into utilizadores(nome, email)
	Values('Diogo Pinto', 'diogo.pinto.t0114469@edu.atec.pt')

-- 2
-- a
--Create Table produtos(
--	cod_produto Int,
--	descricao Varchar(100),
--	stock Int,
--	Constraint pk_produtos_cod_produto Primary Key(cod_produto)
--)

-- b
--Create Table vendas(
--	cod_venda Int Identity(1, 1),
--	data_venda Date,
--	quantidade Int,
--	produto Int,
--	Constraint pk_vendas_cod_venda Primary Key(cod_venda),
--	Constraint fk_vendas_produto Foreign Key(produto) References produtos(cod_produto)
--)

Go

-- b
Create Trigger trg_atualizar_stock_venda
On vendas
After Insert
As
Begin
	Declare @codProduto Int
	Declare @quantidadeVendida Int
	Select @codProduto = i.produto, @quantidadeVendida = i.quantidade From inserted i
	Update produtos
	Set stock = stock - @quantidadeVendida
	Where cod_produto = @codProduto
End

Insert Into produtos(cod_produto, descricao, stock)
	Values(1, 'Produto 01', 20)

Insert Into vendas(data_venda, quantidade, produto)
	Values(GetDate(), 5, 1)

Select *
From produtos
Select *
From vendas

Go

-- c
Create Trigger trg_atualizar_stock_devolucao
On vendas
After Delete
As
Begin
	Declare @codProduto Int
	Declare @quantidade Int
	Select @codProduto = d.produto, @quantidade = d.quantidade From deleted d
	Update produtos
	Set stock = stock + @quantidade
	Where cod_produto = @codProduto
End

Select *
From produtos
Select *
From vendas

Insert Into vendas(data_venda, quantidade, produto)
	Values(GetDate(), 10, 1)

Delete From vendas
Where cod_venda = 2

Go

-- d
Create Trigger trg_atualizar_stock_modificacao
On vendas
After Update
As
Begin
	Declare @codProduto Int
	Declare @quantidadeNova Int
	Declare @quantidadeAntiga Int

	Select @codProduto = d.produto From deleted d
	Select @quantidadeAntiga = d.quantidade From deleted d
	Select @quantidadeNova = i.quantidade From inserted i

	Update produtos
	Set stock = stock + @quantidadeAntiga - @quantidadeNova
	Where cod_produto = @codProduto
End

Select *
From produtos
Select *
From vendas

Update vendas
Set quantidade = 1
Where cod_venda = 1