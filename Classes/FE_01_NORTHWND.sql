Use NORTHWND

-- I
-- 1
Select FirstName, LastName
From Employees
Where Title = 'Sales Manager'

-- 2
Select *
From Employees
Where FirstName Like 'm%'

-- 3
Select FirstName, LastName
From Employees
Order by LastName Desc

-- 4
Select *
From Employees
Where Country <> 'usa'

-- 5
Select *
From Products
Where UnitPrice > 50

-- 6
Select *
From Products
Where UnitPrice Between 30 And 50 And UnitsInStock > 39

-- 7
Select *
From Employees
Where EmployeeID Between 3 And 5
Order by PostalCode

-- 8
Select *
From Employees
Where Country = 'usa' And Title <> 'Sales Representative'

-- II
-- 1
Select CustomerID, CompanyName
From Customers
Order by CompanyName

-- 2
Select CustomerID, CompanyName, ContactName, Region
From Customers
Where Country = 'mexico'
Order by Region

-- 3/4
Select ProductID, ProductName, SupplierID, UnitsInStock, CategoryID
From Products
Where CategoryID = 1 And UnitsInStock > 0
Order by UnitsInStock Desc

-- 5
Select OrderID, OrderDate, CustomerID
From Orders
Order by OrderDate Desc

-- 6
Select OrderID, OrderDate, EmployeeID
From Orders
Where Year(OrderDate) = 1998 And DatePart(weekday, OrderDate) = 6 And EmployeeID = 5
Order by OrderDate Desc

-- 7
Select [Order Details].ProductID, Orders.OrderID, Count([Order Details].ProductID) as 'Total Orders'
From Orders
	Inner Join [Order Details] On Orders.OrderID = [Order Details].OrderID
Where Orders.OrderID In('10812', '10296', '10251')
Group by [Order Details].ProductID, Orders.OrderID

-- 8
Select Orders.OrderID, Sum([Order Details].UnitPrice * [Order Details].Quantity) as 'Total'
From Orders
	Inner Join [Order Details] On Orders.OrderID = [Order Details].OrderID
Where Month(Orders.OrderDate) In(8,9) And Year(Orders.OrderDate) = 1996
Group By Orders.OrderID

-- 9
Select Sum([Order Details].UnitPrice * [Order Details].Quantity) as 'Total wo/ Discount', 
	   Sum(([Order Details].UnitPrice - ([Order Details].UnitPrice * [Order Details].Discount)) * [Order Details].Quantity) as 'Total w/ Discount'
From Orders
	Inner Join [Order Details] On Orders.OrderID = [Order Details].OrderID
Where Orders.CustomerID = 'quick' And DatePart(week, Orders.OrderDate) = 52

-- 10
Select Products.ProductName
From Products
	Inner Join [Order Details] On [Order Details].ProductID = Products.ProductID
	Inner Join Orders On Orders.OrderID = [Order Details].OrderID
Where Orders.CustomerID In('Vinet', 'Hanar') And Orders.EmployeeID = 5 And Year(Orders.OrderDate) = 1996

-- 11
Select Suppliers.SupplierID, Suppliers.CompanyName
From Suppliers
	Inner Join Products On Products.SupplierID = Suppliers.SupplierID
	Inner Join [Order Details] On [Order Details].ProductID = Products.ProductID
	Inner Join Orders On Orders.OrderID = [Order Details].OrderID
	Inner Join Customers On Customers.CustomerID = Orders.CustomerID
Where Year(Orders.OrderDate) = 1996 And Customers.Region = 'sp'
Group by Suppliers.SupplierID, Suppliers.CompanyName