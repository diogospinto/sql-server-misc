--Create Table brand(
--	brand_id Int Identity Primary Key,
--	brand_name Varchar(255) Not Null
--)

--Create Table brand_approvals(
--	brand_id Int Identity Primary Key,
--	brand_name Varchar(255) Not Null
--)

--Create View vw_brands
--As
--Select brand_name, 'Approved' approval_status
--From brand
--Union
--Select brand_name, 'Pending Approval' approval_status
--From brand_approvals

Create Trigger trg_vw_brands
On vw_brands Instead of Insert
As Begin
Set NoCount On
Insert Into brand_approvals(brand_name)
	Select i.brand_name 
	From inserted i
	Where i.brand_name Not In(Select brand_name 
							  From brand)
End

Insert Into vw_brands(brand_name)
	Values('Eddy Merckx')

Select *
From vw_brands