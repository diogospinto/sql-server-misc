Use NORTHWND

Go

-- Grupo I
-- 1
Create Procedure sp_ProdutosClienteData @ClientID Varchar(6), @OrderDate DateTime
As
Begin
	Select Products.ProductID, Products.ProductName, Customers.ContactName, Orders.OrderDate
	From Products
		Inner Join [Order Details] On Products.ProductID = [Order Details].ProductID
		Inner Join Orders On Orders.OrderID = [Order Details].OrderID
		Inner Join Customers On Customers.CustomerID = Orders.CustomerID
	Where Customers.CustomerID Like @ClientID And Orders.OrderDate = @OrderDate
End

Go

Select OrderDate
From Orders

Exec sp_ProdutosClienteData 'Vinet', '1996-07-04'

Go

-- 2
Create Procedure sp_ProdutosClienteData_Between @ClientID Varchar(6), @BeginDate DateTime, @EndDate DateTime
As
Begin
	Select Products.ProductID, Products.ProductName, Customers.ContactName, Orders.OrderDate
	From Products
		Inner Join [Order Details] On Products.ProductID = [Order Details].ProductID
		Inner Join Orders On Orders.OrderID = [Order Details].OrderID
		Inner Join Customers On Customers.CustomerID = Orders.CustomerID
	Where Customers.CustomerID Like @ClientID And Orders.OrderDate Between @BeginDate And @EndDate
End

Exec sp_ProdutosClienteData_Between 'Vinet', '1996-07-04', '1997-07-04'

Go

-- 3
Create Procedure sp_ProdutosFornecedorData @BeginDate DateTime, @EndDate DateTime, @SupplierID Int
As
Begin
	Select Products.ProductID, Products.ProductName, Sum(Products.UnitsInStock) as 'ProdutoTotal'
	From Products
		Inner Join [Order Details] On Products.ProductID = [Order Details].ProductID
		Inner Join Orders On [Order Details].OrderID = Orders.OrderID
	Where Products.SupplierID = @SupplierID And Orders.OrderDate Between @BeginDate And @EndDate
	Group By Products.ProductID, Products.ProductName, Products.UnitsInStock
End

Go

Exec sp_ProdutosFornecedorData '1996-07-04', '1996-07-15', 5

Go

-- Grupo II
-- 1
Create Table Audit(
	DataReg DateTime Constraint pk_Audit_DataReg Primary Key,
	Operacao Varchar(10) Constraint nn_Audit_Operacao Not Null,
	Registo Varchar(50) Constraint nn_Audit_Registo Not Null,
	Tabela Varchar(20) Constraint nn_Audit_Tabela Not Null
)

Go

-- 1
Create Trigger trg_InsertRemove_Customers
On Customers
After Insert, Delete
As
Begin
	Insert Into Audit(DataReg, Operacao, Registo, Tabela)
		Select GetDate(), 'Insert', i.CustomerID, 'Customers'
		From inserted i
		Union All
		Select GetDate(), 'Delete', d.CustomerID, 'Customers'
		From deleted d
End

Insert Into Customers(CustomerID, CompanyName)
	Values('XPTO1', 'Nome do XPTO1')

Go

Insert Into Customers(CustomerID, CompanyName)
Values('XPTO2', 'Nome do XPTO2')

Go

Insert Into Customers(CustomerID, CompanyName)
Values('XPTO3', 'Nome do XPTO3')

Go

Insert Into Customers(CustomerID, CompanyName)
Values('XPTO4', 'Nome do XPTO4')

Go

Select *
From Customers

Select *
From Audit

Delete From Customers
Where Customers.CustomerID Like 'xpto4'