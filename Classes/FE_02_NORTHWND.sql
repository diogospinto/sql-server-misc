-- I
-- 1
Select CustomerID, CompanyName, Region
From Customers
Where Region Is Null

-- 2
Update Customers
Set Region = '?'
Where Region Is Null

-- 3
Update Customers
Set City = 'Lisbon', Region = 'Estremadura'
Where City = 'Lisboa' And Region = '?'

-- 4
Insert Into Shippers(CompanyName, Phone)
	Values ('Despacho Expresso, SA', 7563884340),
		   ('Despachar ou n�o despachar, Lda', 4398732412)

-- 5
Delete From Customers
Where City = 'Braga'

-- 6
Delete From [Order Details]
Where [Order Details].OrderID In (Select Orders.OrderID 
								  From Orders, Employees, [Order Details] 
								  Where Orders.OrderID = [Order Details].OrderID And Employees.EmployeeID = Orders.EmployeeID 
								  And Employees.Region Is Not Null)

Go

Delete From Orders
Where EmployeeID In (Select Employees.EmployeeID From Employees, Orders Where Orders.EmployeeID = Employees.EmployeeID And Employees.Region Is Not Null)

Go

Delete From EmployeeTerritories
Where EmployeeTerritories.EmployeeID In (Select Employees.EmployeeID 
										 From Employees 
										 Where Employees.Region Is Not Null)

Go

Alter Table Employees
Drop Constraint FK_Employees_Employees

Go

Delete From Employees
Where Region Is Not Null

Select *
From Employees

-- 7
Select Suppliers.SupplierID, Suppliers.CompanyName
From Suppliers
	Inner Join Products On Products.SupplierID = Suppliers.SupplierID
	Inner Join [Order Details] On [Order Details].ProductID = Products.ProductID
	Inner Join Orders On [Order Details].OrderID = Orders.OrderID
	Inner Join Customers On Orders.CustomerID = Customers.CustomerID
Where Year(Orders.OrderDate) = 1996 And Customers.City In('Lisboa', 'Lisbon')

Go

-- 8
Create View LisbonProductSuppliersIn1996
As
Select Suppliers.SupplierID, Suppliers.CompanyName
From Suppliers
	Inner Join Products On Products.SupplierID = Suppliers.SupplierID
	Inner Join [Order Details] On [Order Details].ProductID = Products.ProductID
	Inner Join Orders On [Order Details].OrderID = Orders.OrderID
	Inner Join Customers On Orders.CustomerID = Customers.CustomerID
Where Year(Orders.OrderDate) = 1996 And Customers.City In('Lisboa', 'Lisbon')

Go

-- 9
Select *
From LisbonProductSuppliersIn1996
Order By CompanyName

-- 10 / 11
Select *
Into Fornecedores_Lisboa
From LisbonProductSuppliersIn1996

Go

-- 12
Alter View LisbonProductSuppliersIn1996
As
Select Sum(Quantity) as 'QuantitySum', Avg(Quantity) as 'QuantityAvg', Max(Quantity) as 'MaxQuantity', Suppliers.SupplierID, Suppliers.CompanyName
From Suppliers
	Inner Join Products On Products.SupplierID = Suppliers.SupplierID
	Inner Join [Order Details] On [Order Details].ProductID = Products.ProductID
	Inner Join Orders On [Order Details].OrderID = Orders.OrderID
	Inner Join Customers On Orders.CustomerID = Customers.CustomerID
Where Year(Orders.OrderDate) = 1996 And Customers.City In('Lisboa', 'Lisbon')
Group by Suppliers.SupplierID, Suppliers.CompanyName

Go

-- 13
Create Index CustomersCompanyName
On Customers(CompanyName)

-- 14
Drop Table Fornecedores_Lisboa