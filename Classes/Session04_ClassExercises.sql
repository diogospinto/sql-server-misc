-- 2
Create Proc sp_Insert_Categories_1 @CategoryID int, @CategoryName Varchar(12), @CategoryDesc Nvarchar(Max)
As
Begin
	Declare @ID int
	Set @ID = (Select CategoryID From Categories Where CategoryName Like @CategoryName)

	If @ID Is Null
	Begin
		Insert Into Categories(CategoryID, CategoryName, Description) Values(@CategoryID, @CategoryName, @CategoryDesc)
	End
	Else
	Begin
		Select 'J� existe a categoria' as 'Error'
	End
End

Select *
From Categories

--Set Identity_Insert Categories On

Exec sp_Insert_Categories_1 10, 'Produto', 'food item consisting of one or more types of food placed'

Go

-- 3
Create Procedure sp_Product_Not_Available
As
Begin
	Select ProductID, ProductName, UnitPrice
	From Products
	Where UnitsInStock = 0
End

Select *
From Products
Order by UnitsInStock Asc

-- 4
Exec sp_Product_Not_Available

-- 5
Select *
From Orders
Select *
From [Order Details]

Go

Create Procedure sp_Vendas_Produtos_Ano1996
As
Begin
	Select Products.ProductName, Count([Order Details].OrderID) as 'Total de Vendas', Year(Orders.OrderDate) as 'Ano de Venda'
	From Products, Orders, [Order Details]
	Where [Order Details].OrderID = Orders.OrderID And [Order Details].ProductID = Products.ProductID
	Group by Products.ProductName, Year(Orders.OrderDate)
	Having Year(Orders.OrderDate) = 1996
	Order by Count([Order Details].OrderID) Asc
End

Exec sp_Vendas_Produtos_Ano1996

Go

-- 6
Create Procedure sp_Vendas_Produtos_Ano @Ano int
As
Begin
	Select Products.ProductName, Sum(([Order Details].Quantity * [Order Details].UnitPrice) * (1 - [Order Details].Discount)) as 'Pre�o Total', Count(*) as 'Total de Vendas', Year(Orders.OrderDate) as 'Ano de Venda'
	From Products, Orders, [Order Details]
	Where [Order Details].OrderID = Orders.OrderID And [Order Details].ProductID = Products.ProductID
	Group by Products.ProductName, Year(Orders.OrderDate)
	Having Year(Orders.OrderDate) = @Ano
	Order by Count([Order Details].OrderID) Asc
End

Go

Exec sp_Vendas_Produtos_Ano '1996'

Go

Create Procedure sp_Cliente_Com_Maior_Volume_Compras
As
Begin
	Select Top 1 Customers.CustomerID, Customers.ContactName, Count(Orders.CustomerID) as 'Total Orders'
	From Customers, Orders
	Where Customers.CustomerID = Orders.CustomerID
	Group by Customers.CustomerID, Customers.ContactName
	Order by Count(Orders.CustomerID) Desc
End

Go

Exec sp_Cliente_Com_Maior_Volume_Compras

-- Grupo II
Create Table Livros(
	codLivro Int,
	titulo Varchar(20),
	editora Varchar(30),
	edicao Varchar(2),
	preco Money
)

Go

Create Table Autores(
	codLivro Int,
	codAutor Int
)

Go

Create Table Autor(
	codAutor Int,
	nome Varchar(20),
	Nacionalidade Varchar(20)
)

Go

-- 1
Create Procedure sp_Todos_Os_Autores
As
Begin
	Select *
	From Autor
	Where nome Like 'a%'
End

Exec sp_Todos_Os_Autores

Go

-- 2
Create Procedure sp_Quantos_Livros @CodAutor Int
As
Begin
	Select Count(*) as 'Total de Livros'
	From Autores
	Where Autores.codAutor = @CodAutor
End

Go

Exec sp_Quantos_Livros '21'

Go

-- 3
Create Procedure sp_Total_Preco_por_Autor @CodAutor Int
As
Begin
	Select Autor.codAutor, Sum(Livros.preco) as 'Total Pre�o'
	From Autores, Livros, Autor
	Where Autores.codLivro = Livros.codLivro And Autor.codAutor = Autores.codAutor And Autores.codAutor = @CodAutor
	Group by Autor.codAutor
End

Go

Exec sp_Total_Preco_por_Autor '21'

Go

-- 4
Create Procedure sp_Inserir_Autor @CodAutor Int, @Nome Varchar(20), @Nacionalidade Varchar(20)
As
Begin
	If Not Exists(Select codAutor
				  From Autor
				  Where codAutor = @CodAutor)
		Insert Into Autor(codAutor, nome, Nacionalidade)
			Values(@CodAutor, @Nome, @Nacionalidade)
	Else
		Select 'C�digo de autor j� existente!'
End

Go

Exec sp_Inserir_Autor 21, 'Autor 01', 'Portugu�s'
Exec sp_Inserir_Autor 22, 'Autor 02', 'Portugu�s'

Go

-- 5
Create Procedure sp_Atualizar_Livro @CodLivro Int, @Preco Money
As
Begin
	If Exists(Select codLivro
			  From Livros
			  Where codLivro = @CodLivro)
		Update Livros
		Set preco = @Preco
		Where codLivro = @CodLivro
	Else
		Select 'Livro n�o existe!' as 'Aten��o'
End

Go

Insert Into Livros(codLivro, titulo, preco)
	Values(99, 'Deus Irae', 10)

Select *
From Livros

Go

Exec sp_Atualizar_Livro 99, 15.45

Go

-- 6
Create Procedure sp_Eliminar_Autor @codAutor Int
As
Begin
	If Not Exists(Select codAutor
				  From Autor
				  Where codAutor = @codAutor)
		Select 'C�digo de autor n�o existe!' as 'Aten��o!'
	Else
		Delete From Autores
		Where codAutor = @codAutor
		Delete From Autor
		Where codAutor = @codAutor
End

Go

Select *
From Autor
Select *
From Livros
Select *
From Autores

Insert Into Autores(codAutor, codLivro)
	Values(21, 99)

Go

Exec sp_Eliminar_Autor 21