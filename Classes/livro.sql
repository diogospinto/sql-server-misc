--Create Database sql_session06

--Go

Drop Database sql_session06

Go

if exists(select 1 from sys.sysforeignkey where role='FK_AUTORIA_AUTORIA_AUTOR') then
    alter table Autoria
       delete foreign key FK_AUTORIA_AUTORIA_AUTOR
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_AUTORIA_AUTORIA_LIVRO') then
    alter table Autoria
       delete foreign key FK_AUTORIA_AUTORIA_LIVRO
end if;

create table Autor 
(
    IDAutor              text                        not null,
    Nome                 text,
    Nacionalidade        text,
    primary key (IDAutor)
)

Go

create table Autoria 
(
    IDLivro              integer                        not null,
    IDAutor              text                        not null,
    primary key (IDLivro, IDAutor)
)

Go

create  index AUTORIA_FK on Autoria (
IDLivro ASC
)

Go

create  index AUTORIA_FK2 on Autoria (
IDAutor ASC
)

Go

create table Livro 
(
    IDLivro              integer                        not null,
    Titulo               text,
    Editora              text,
    Edicao               integer,
    primary key (IDLivro)
);

alter table Autoria
   add foreign key FK_AUTORIA_AUTORIA_AUTOR (IDAutor)
      references Autor (IDAutor)
      on update restrict
      on delete restrict;

alter table Autoria
   add foreign key FK_AUTORIA_AUTORIA_LIVRO (IDLivro)
      references Livro (IDLivro)
      on update restrict
      on delete restrict;

