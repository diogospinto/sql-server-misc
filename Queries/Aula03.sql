-- 1) Mostrar, numa coluna, o t�tulo de cada CD e de cada uma das m�sicas;
Select Musica.interprete + ': ' + CD.titulo + ' - ' + Musica.titulo as 'Int�rprete: CD - M�sica'
From CD, Musica
Where CD.cod_cd = Musica.cod_cd
Order by Musica.interprete, CD.titulo, Musica.titulo

-- 2) Copiar e alterar o comando da al�nea anterior, de modo a apresentar tamb�m o comprimento de
	-- cada t�tulo e por ordem decrescente;
Select CD.titulo + ' - ' + Musica.titulo as 'CD - M�sica', Len(CD.titulo) as 'CD: T�tulo', Len(Musica.titulo) as 'M�sica: T�tulo'
From CD, Musica
Where CD.cod_cd = Musica.cod_cd
Order by Len(CD.titulo) Desc, Len(Musica.titulo) Desc

-- 3) Mostrar a dura��o das m�sicas dos Pink Floyd que s�o iguais � dura��o de m�sicas de outros int�rpretes;
Select Musica.duracao, Musica.titulo
From Musica
Where Musica.interprete = 'Pink Floyd' And Musica.duracao In ((Select M2.duracao 
															From Musica M2
															Where M2.interprete <> 'Pink Floyd'))
Order by Musica.duracao

-- 4) Alterar o comando da al�nea anterior, de modo a mostrar a dura��o das m�sicas por ordem decrescente;
Select Musica.duracao, Musica.titulo
From Musica
Where Musica.interprete = 'Pink Floyd' And Musica.duracao In ((Select M2.duracao
																From Musica M2
																Where M2.interprete <> 'Pink Floyd'))
Order by Musica.duracao Desc

-- 5) Mostrar o id das editoras que n�o est�o relacionadas com qualquer CD;
Select Editora.id_editora
From Editora
Where Not Exists (Select CD.cod_cd
					From CD
					Where CD.id_editora = Editora.id_editora)

-- 6) Alterar o comando da al�nea anterior, de modo a mostrar o resultado por ordem decrescente.
Select Editora.id_editora
From Editora
Where Not Exists (Select CD.cod_cd
					From CD
					Where CD.id_editora = Editora.id_editora)
Order by Editora.id_editora Desc

-- 7) Mostrar apenas a quantidade de CD comprados por local de compra e o respetivo local de compra;
Select Count(CD.cod_cd) as 'Qtd de CDs', CD.local_compra as 'Local de Compra'
From CD
Group by CD.local_compra

-- 8) Copiar e alterar o comando da al�nea anterior, de forma a mostrar o resultado por ordem crescente da quantidade de CD comprados;
Select Count(CD.cod_cd) as 'Qtd de CDs', CD.local_compra as 'Local de Compra'
From CD
Group by CD.local_compra
Order by Count(CD.cod_cd)

-- 9) Copiar e alterar o comando da al�nea anterior, de forma a n�o mostrar nulos;
Select Count(CD.cod_cd) as 'Qtd de CDs', CD.local_compra as 'Local de Compra'
From CD
Where CD.local_compra Is Not Null
Group by CD.local_compra
Order by Count(CD.cod_cd)

-- 10) Copiar e alterar o comando da al�nea anterior, de forma a mostrar tamb�m, para cada local de
	-- compra, o valor total pago e o maior valor pago;
Select Count(CD.cod_cd) as 'Qtd de CDs', CD.local_compra as 'Local de Compra', 
Sum(CD.valor_pago) as 'Total Valor Pago', Max(CD.valor_pago) as 'M�x. Valor Pago'
From CD
Where CD.local_compra Is Not Null
Group by CD.local_compra
Order by Count(CD.cod_cd)

-- 11) Mostrar, para cada CD e respetivos int�rpretes, a quantidade de m�sicas do CD em que o
	-- int�rprete participa. Al�m da quantidade referida, tamb�m deve ser apresentado o c�digo do CD e o int�rprete;
Select Distinct CD.titulo as 'T�tulo', Musica.interprete as 'Int�rprete', CD.cod_cd as 'C�d. CD', (Select Count(*)
												From Musica M2
												Where M2.cod_cd = Musica.cod_cd And M2.interprete = Musica.interprete
												Group by M2.interprete) as 'M�sicas Em Que Participa'
From CD, Musica
Where CD.cod_cd = Musica.cod_cd
Order by 'M�sicas Em Que Participa'

-- 12) Copiar e alterar o comando da al�nea anterior, de modo a mostrar apenas, o c�digo do CD e o int�rprete;
Select Distinct CD.cod_cd, Musica.interprete
From CD, Musica
Where CD.cod_cd = Musica.cod_cd
Order by CD.cod_cd, Musica.interprete

-- 13) Copiar e alterar o comando da al�nea anterior, de modo a mostrar apenas o int�rprete;
Select Distinct Musica.interprete
From CD, Musica
Where CD.cod_cd = Musica.cod_cd

-- 14) Mostrar a quantidade de CD comprados em cada local de compra;
Select Count(CD.cod_cd) as 'Qtd de CDs', CD.local_compra as 'Local de Compra'
From CD
Group by CD.local_compra

-- 15) Alterar o comando da al�nea anterior, de modo a mostrar apenas as quantidades superiores a 2;
Select Count(CD.cod_cd) as 'Qtd de CDs', CD.local_compra as 'Local de Compra'
From CD
Group by CD.local_compra
Having Count(CD.cod_cd) > 2

-- 16) Mostrar os locais de compra, cujo m�dia do valor pago por CD � inferior a 10, juntamente com o respetivo total do valor pago.
Select CD.local_compra as 'Local de Compra', Avg(CD.valor_pago) as 'M�dia Valor Pago', Sum(CD.valor_pago) as 'Total de Valor Pago'
From CD
Group by CD.local_compra
Having Avg(CD.valor_pago) < 10

-- 17) Mostrar o valor total pago nos locais de compra, cuja quantidade de CD comprados � inferior a 2.
	-- O local de compra tamb�m deve ser visualizado;
Select Sum(CD.valor_pago) as 'Total de Valor Pago', CD.local_compra as 'Local de Compra', Count(CD.cod_cd) as 'Qtd'
From CD
Group by CD.local_compra
Having Count(CD.cod_cd) < 2

-- 18) Mostrar o int�rprete e o c�digo do CD em que o int�rprete participa apenas em 1 m�sica. O
	-- resultado deve ser apresentado por ordem crescente do c�digo do CD e, em caso de igualdade, por ordem alfab�tica do int�rprete;
Select Musica.interprete, Musica.cod_cd
From Musica
Group by Musica.interprete, Musica.cod_cd
Having Count(*) = 1
Order by Musica.cod_cd, Musica.interprete

-- 19) Copiar e alterar o comando da al�nea anterior, de modo a mostrar apenas os int�rpretes e sem duplicados;
Select Distinct Musica.interprete
From Musica, CD
Where CD.cod_cd = Musica.cod_cd And (Select Count(*)
										From CD C, Musica M
										Where C.cod_cd = M.cod_cd And M.interprete = Musica.interprete
										Group by interprete) = 1

-- 20) Copiar e alterar o comando da al�nea anterior, de modo a mostrar apenas os int�rpretes come�ados por E ou L;
Select Distinct Musica.interprete
From Musica, CD
Where CD.cod_cd = Musica.cod_cd And (Select Count(*)
										From CD C, Musica M
										Where C.cod_cd = M.cod_cd And Musica.interprete = M.interprete
										Group by interprete) = 1
								And Musica.interprete Like '[EL]%'

-- 21) Mostrar, para cada CD, o t�tulo e a quantidade de m�sicas;
Select CD.titulo as 'T�tulo', (Select Count(*)
					From Musica
					Where CD.cod_cd = Musica.cod_cd) as 'Qtd de M�sicas'
From CD
Order by 'Qtd de M�sicas' Desc

-- 22) Mostrar, para cada CD, o c�digo, o t�tulo e a quantidade de m�sicas;
Select CD.titulo, CD.cod_cd, (Select Count(*)
								From Musica
								Where CD.cod_cd = Musica.cod_cd) as 'Qtd de M�sicas'
From CD
Order by 'Qtd de M�sicas' Desc

-- 23) Mostrar, para cada CD, o c�digo, o t�tulo e a quantidade de m�sicas cuja dura��o de pelo menos uma das m�sicas seja superior a 5;
Select CD.cod_cd, CD.titulo, (Select Count(*)
								From Musica
								Where CD.cod_cd = Musica.cod_cd) as 'Qtd de M�sicas'
From CD
Where (Select Count(*)
		From Musica
		Where CD.cod_cd = Musica.cod_cd And Musica.duracao > 5) > 0

-- 24) Mostrar, para cada CD com menos de 6 m�sicas, o c�digo, o t�tulo e a quantidade de m�sicas do CD;
Select CD.cod_cd, CD.titulo, (Select Count(*)
								From Musica
								Where CD.cod_cd = Musica.cod_cd) as 'Qtd de M�sicas'
From CD
Where (Select Count(*)
		From Musica
		Where CD.cod_cd = Musica.cod_cd) < 6

-- 25) Mostrar, para cada CD cujas m�sicas t�m uma dura��o m�dia superior a 4, o c�digo, o t�tulo e a quantidade de m�sicas do CD.
Select CD.cod_cd, CD.titulo, (Select Count(*)
								From Musica
								Where CD.cod_cd = Musica.cod_cd) as 'Qtd de M�sicas'
From CD
Where Exists (Select *
			From Musica
			Where CD.cod_cd = Musica.cod_cd
			Group by Musica.cod_cd
			Having Avg(Musica.duracao) > 4)