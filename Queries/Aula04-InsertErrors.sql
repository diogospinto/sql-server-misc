-- Testes
-- a. Tabela Automovel
-- i. Registo(s) com matricula com formato inv�lido;
Insert Into Automovel (matricula, marca) 
				Values ('AA-AA-12', 'Lambo')

-- ii. Registo(s) com matricula existente na tabela. Note que esta situa��o ocorre sempre com chaves prim�rias;
Insert Into Automovel (matricula, marca) 
				Values ('XO-65-98', 'Lambo')

-- iii. Registo(s) com matricula NULL;
Insert Into Automovel (matricula, marca) 
				Values (Null, 'Lambo')

-- iv. Registo(s) com marca NULL;
Insert Into Automovel (matricula, marca) 
				Values ('11-11-AA', Null)

-- v. Registo(s) com cilindrada inv�lida;
Insert Into Automovel (cilindrada, matricula, marca) 
				Values (1, '11-11-AA', 'Rari')

-- vi. Registo(s) com ano_fabrico inv�lidos;
Insert Into Automovel (ano_fabrico, matricula, marca) 
				Values (1771, '11-11-AA', 'Rari')

-- vii. Registo(s) com preco_venda inv�lidos.
Insert Into Automovel (preco_venda, matricula, marca) 
				Values (-1, '11-11-AA', 'Rari')

-- b. Tabela Cliente
-- i. Registo(s) com um id_cliente inexistente na tabela;
Insert Into Cliente (id_cliente) 
				Values (7)

-- ii. Registo(s) com id_cliente NULL;
Insert Into Cliente (id_cliente) 
				Values (Null)

-- iii. Registo(s) com nome NULL;
Insert Into Cliente (nome)
				Values (Null)

-- iv. Registo(s) com nr_identificacao_civil existente na tabela. Note que esta situa��o ocorre sempre com campos UNIQUE;
Insert Into Cliente (nr_identificacao_civil, data_nascimento, nome, nif) 
				Values (123456, '1982-12-01', 'Nome', 123456789)

-- v. Registo(s) com nr_identificacao_civil inv�lido, excluindo o caso da al�nea anterior;
Insert Into Cliente (nr_identificacao_civil, data_nascimento, nome, nif) 
				Values (123, '1982-12-01', 'Nome', 123456789)

-- vi. Registo(s) com nif existente na tabela;
Insert Into Cliente (nif, data_nascimento, nome) 
				Values (105098124, '1982-12-01', 'Nome')

-- vii. Registo(s) com nif NULL;
Insert Into Cliente (nif, data_nascimento, nome) 
				Values (Null, '1982-12-01', 'Nome')

-- viii. Registo(s) com nif inv�lido, excluindo os casos das duas al�neas anteriores;
Insert Into Cliente (nif, data_nascimento, nome, nr_identificacao_civil) 
				Values (1234567890, '1982-12-01','Nome', 1234567)

-- ix. Registo(s) com data_nascimento inv�lida.
Insert Into Cliente (data_nascimento, nome) 
				Values (Null, 'Nome')

-- c. Tabela Automovel_Cliente
-- i. Registo(s) com chave prim�ria existente na tabela;
Insert Into Automovel_Cliente (matricula, id_cliente) 
				Values ('42-90-AS', 2)

-- ii. Registo(s) com campo(s) NULL;
Insert Into Automovel_Cliente (matricula, id_cliente) 
				Values (Null, 2)

Insert Into Automovel_Cliente (matricula, id_cliente) 
				Values ('42-90-AS', Null)

-- iii. Registo(s) com id_cliente ou matricula inexistente na tabela Cliente ou Automovel,
	-- respetivamente. Note que esta situa��o ocorre sempre com chaves estrangeiras.
Insert Into Automovel_Cliente (matricula, id_cliente) 
				Values ('42-90-AA', 4)

Insert Into Automovel_Cliente (matricula, id_cliente) 
				Values ('42-90-AS', 9)

-- d. Tabela Revisao
-- i. Registo(s) com chave prim�ria existente na tabela;
Insert Into Revisao (matricula, data_hora_marcacao, efectuada) 
				Values ('42-90-AS', '2018-10-23 10:50', 'N')

-- ii. Registo(s) com campo(s) NULL na chave prim�ria;
Insert Into Revisao (matricula, data_hora_marcacao, efectuada) 
				Values ('42-90-AS', Null, 'N')

Insert Into Revisao (matricula, data_hora_marcacao, efectuada) 
				Values (Null, '2018-10-23 10:50', 'N')

-- iii. Registo(s) com matricula inexistente na tabela Automovel;
Insert Into Revisao (matricula, data_hora_marcacao, efectuada) 
				Values ('11-22-TR', '2018-10-23 10:50', 'N')

-- iv. Registo(s) com o campo efetuada diferente de S ou N;
Insert Into Revisao (matricula, data_hora_marcacao, efectuada) 
				Values ('42-90-AS', '2018-10-23 11:00', 'A')

-- v. Registo(s) com o campo efetuada a NULL.
Insert Into Revisao (matricula, data_hora_marcacao, efectuada) 
				Values ('42-90-AS', '2018-10-23 10:50', Null)

-- 8 - Substitui��o de Chave Prim�ria
Alter Table Revisao
Drop Constraint pk_Revisao_matricula_data_hora_marcacao

Alter Table Revisao
Add id_revisao Int Identity(1,1) Constraint pk_Revisao_id_revisao Primary Key

Insert Into Revisao (matricula, data_hora_marcacao, efectuada) 
				Values ('42-90-AS', '2018-10-23 10:50', 'N')

Select *
From Revisao

Delete From Revisao
Where id_revisao = 7

Alter Table Revisao
Add Constraint un_Revisao_matricula_data_hora_marcacao Unique (matricula, data_hora_marcacao)

Insert Into Revisao (matricula, data_hora_marcacao, efectuada) 
				Values ('42-90-AS', '2018-10-23 10:50', 'N')