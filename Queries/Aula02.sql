Use Campeonato
Go

-- a. Sele��es simples
-- i. Mostrar todos os dados da tabela Equipas;
Select *
From Equipa

-- ii. Mostrar todos os dados da equipa com o id igual a 12;
Select *
From Equipa
Where id_equipa = 12

-- iii. Mostrar o id e o nome de todas as equipas;
Select Equipa.id_equipa as ID, Equipa.nome as Nome
From Equipa

-- iv. Mostrar o id, o nome e a idade dos treinadores com menos de 40 anos de idade;
Select id_treinador, nome, idade
From Treinador
Where idade < 40

-- v. Mostrar todos os dados da tabela Experiencias relativos aos treinadores que 
	-- treinaram juniores ou que tenham mais do que 10 anos de experi�ncia;
Select *
From Experiencia
Where escalao Like 'jun%' Or anos > 10

-- vi. Mostrar todos os dados dos treinadores com idade pertencente ao intervalo [45, 53] e por ordem decrescente da idade;
Select *
From Treinador
Where idade Between 45 And 53
Order by idade Desc

-- vii. Mostrar todos os dados das bolas dos fabricantes Reebok e Olimpic;
Select *
From Bola
Where fabricante Like 'Reebok' Or fabricante Like 'Olimpic'

-- viii. Mostrar todos os dados dos treinadores cujo nome come�a pela letra A.
Select *
From Treinador
Where nome Like 'a%'

-- b. Fun��es de agrega��o
-- i. Mostrar a quantidade de equipas que disputam o campeonato;
Select Count(Equipa.id_equipa) as 'Qtd'
From Equipa

-- ii. Mostrar a quantidade de fabricantes distintos que produzem bolas usadas no campeonato;
Select Count(Distinct fabricante) as 'Qtd Fabricantes'
From Bola

-- iii. Mostrar a quantidade de treinadores com idade superior a 40 anos
Select Count(Distinct Treinador.id_treinador) as 'Treinadores c/ idade > 40'
From Treinador
Where idade > 40

-- c. Sele��es em m�ltiplas tabelas � Jun��es (joins)
-- i. Mostrar o id das equipas que utilizam bolas do fabricante Adidas;
Select Equipa.id_equipa as 'ID Equipa', Bola.fabricante as 'Fabricante', Equipa.nome as 'Nome Equipa'
From Equipa, Bola
Where Equipa.id_equipa=Bola.id_equipa And Bola.fabricante = 'Adidas'

-- ii. Mostrar o resultado da al�nea anterior, mas sem repeti��es;
Select Distinct Equipa.id_equipa as 'ID Equipa', Bola.fabricante as 'Fabricante', Equipa.nome as 'Nome Equipa'
From Equipa, Bola
Where Equipa.id_equipa=Bola.id_equipa And Bola.fabricante = 'Adidas'

-- iii. Mostrar a m�dia das idades dos treinadores de juvenis;
Select Avg(Treinador.idade) as 'M�dia Idade'
From Treinador, Experiencia
Where Treinador.id_treinador = Experiencia.id_treinador And Experiencia.escalao Like 'juvenis'

-- iv. Mostrar todos os dados dos treinadores que treinaram juniores durante 5 ou mais anos;
Select Treinador.*, Experiencia.escalao, Experiencia.anos
From Treinador, Experiencia
Where Treinador.id_treinador = Experiencia.id_treinador And Experiencia.escalao Like 'jun%' And Experiencia.anos >= 5

-- v. Mostrar todos os dados dos treinadores e das equipas por eles treinadas;
Select Distinct Treinador.*, Equipa.*
From Treinador, Equipa, Experiencia
Where Treinador.id_treinador = Experiencia.id_treinador And Equipa.id_equipa=Experiencia.id_equipa

-- vi. Mostrar os nomes e os telefones dos treinadores e os nomes das equipas por eles treinadas;
Select Distinct Treinador.nome, Treinador.telefone, Equipa.nome
From Treinador, Equipa, Experiencia
Where Treinador.id_treinador = Experiencia.id_treinador And Equipa.id_equipa = Experiencia.id_equipa

-- vii. Mostrar todos os dados da equipa do Acad�mico e dos respetivos treinadores;
Select Distinct Equipa.*, Treinador.*
From Treinador, Equipa, Experiencia
Where Treinador.id_treinador = Experiencia.id_treinador And Equipa.id_equipa = Experiencia.id_equipa 
And Equipa.nome Like 'acad�mico'

-- viii. Mostrar a idade do treinador mais velho do Acad�mico;
Select Distinct Top(1) Treinador.idade as 'Idade Treinador Mais Velho'
From Treinador, Equipa, Experiencia
Where Treinador.id_treinador = Experiencia.id_treinador And Experiencia.id_equipa = Equipa.id_equipa 
And Equipa.nome Like 'Acad�mico'
Order by Treinador.idade Desc

-- ix. Mostrar o total de anos de experi�ncia do treinador Ant�nio do Acad�mico
Select Sum(Experiencia.anos) as 'Anos de Experi�ncia'
From Treinador, Experiencia, Equipa
Where Treinador.id_treinador = Experiencia.id_treinador And Equipa.id_equipa = Experiencia.id_equipa 
And Treinador.nome Like 'Ant�nio' And Equipa.nome Like 'Acad�mico'