-- a. Sele��es simples
-- i. Mostrar todos os dados da tabela CD;
Select *
From CD

-- ii. Mostrar o t�tulo e a data de compra de todos os CD;
Select titulo, data_compra
From CD

-- iii. Mostrar a data de compra de todos CD;
Select data_compra
From CD

-- iv. Mostrar o resultado da al�nea anterior, mas sem repeti��es;
Select Distinct data_compra
From CD

-- v. Mostrar o c�digo do CD e o int�rprete de todas as m�sicas;
Select cod_cd, interprete
From Musica

-- vi. Mostrar o resultado da al�nea anterior, mas sem repeti��es;
Select Distinct cod_cd, interprete
From Musica

-- vii. Mostrar a data de compra de todos os CD com o resultado intitulado "Data de Compra";
Select data_compra as 'Data de Compra'
From CD

-- viii. Mostrar o t�tulo, o valor pago e o respetivo valor do IVA de todos os CD. O valor do
	-- IVA � calculado de acordo com a seguinte f�rmula: valor do IVA = valor pago * 0.23;
Select titulo as 'T�tulo', valor_pago as 'Valor Pago', valor_pago * 0.23 as 'Valor do IVA'
From CD

-- ix. Mostrar todos os dados de todas as m�sicas do CD com o c�digo 2;
Select *
From Musica
Where cod_cd = 2

-- x. Mostrar todos os dados de todas as m�sicas que n�o pertencem ao CD com o c�digo 2;
Select *
From Musica
Where cod_cd <> 2

-- xi. Mostrar todos os dados de todas as m�sicas do CD com o c�digo 2 cuja dura��o seja superior a 5;
Select *
From Musica
Where cod_cd = 2 And duracao > 5

-- xii. Mostrar todos os dados das m�sicas do CD com o c�digo 2 cuja dura��o perten�a ao intervalo [4,6];
Select *
From Musica
Where cod_cd = 2 And duracao Between 4 And 6

-- xiii. Mostrar todos os dados das m�sicas do CD com o c�digo 2 cuja dura��o seja inferior a 4 ou superior a 6;
Select *
From Musica
Where cod_cd = 2 And duracao < 4 Or duracao > 6

-- xiv. Mostrar todos os dados das m�sicas com os n�meros: 1, 3, 5 ou 6;
Select *
From Musica
Where nr_musica In (1, 3, 5, 6)
Order by nr_musica

-- xv. Mostrar todos os dados das m�sicas com os n�meros diferentes de 1, 3, 5 e 6;
Select *
From Musica
Where nr_musica Not In (1, 3, 5, 6)
Order by nr_musica

-- xvi. Mostrar os t�tulos dos CD comprados na FNAC;
Select titulo as 'T�tulo', local_compra
From CD
Where local_compra Like 'Fnac'

-- xvii. Mostrar os t�tulos dos CD que n�o foram comprados na FNAC;
Select titulo as 'T�tulo', local_compra
From CD
Where local_compra Not Like 'Fnac'

-- xviii. Mostrar todos os dados das m�sicas cujo int�rprete � uma orquestra;
Select *
From Musica
Where interprete Like '%Orquestra%' Or interprete Like '%Orchestra%'

-- xix. Mostrar todos os dados das m�sicas cujo int�rprete tem um Y;
Select *
From Musica
Where interprete Like '%y%'

-- xx. Mostrar todos os dados das m�sicas cujo nome termina com DAL?, sendo ? qualquer car�ter;
Select *
From Musica
Where titulo Like '%DAL_'

-- xxi. Mostrar todos os dados das m�sicas cujo t�tulo tem o car�ter %;
Select *
From Musica
Where titulo Like '%[%]%'

-- xxii. Mostrar todos os dados das m�sicas cujo t�tulo � iniciado pela letra B, D ou H;
Select *
From Musica
Where titulo Like '[bdh]%'
Order by titulo

-- xxiii. Mostrar todos os dados dos CD sem o local de compra registado;
Select *
From CD
Where local_compra Is Null

-- xxiv. Mostrar todos os dados dos CD com o local de compra registado.
Select *
From CD
Where local_compra Is Not Null

-- b. Ordena��es
-- i. Mostrar o t�tulo e a data de compra dos CD, por ordem alfab�tica do t�tulo do CD;
Select titulo, data_compra
From CD
Order by titulo

-- ii. Mostrar o t�tulo e a data de compra dos CD, por ordem descendente da data de compra do CD;
Select titulo, data_compra
From CD
Order by data_compra Desc

-- iii. Mostrar o t�tulo e o local de compra dos CD, por ordem ascendente do local de compra do CD;
Select titulo, data_compra, local_compra
From CD
Order by local_compra

-- iv. Mostrar o resultado da al�nea anterior, mas por ordem descendente do local de compra do CD;
Select titulo, data_compra, local_compra
From CD
Order by local_compra Desc

-- v. Mostrar o t�tulo, o valor pago e o respetivo valor do IVA dos CD, por ordem decrescente do IVA;
Select titulo as 'T�tulo', valor_pago as 'Valor Pago', valor_pago * 0.23 as 'Valor do IVA'
From CD
Order by 'Valor do IVA' Desc

-- vi. Mostrar o t�tulo do CD por ordem descendente da data de compra e, no caso da igualdade de datas, por ordem alfab�tica do t�tulo.Select titulo, data_compraFrom CDOrder by data_compra Desc, titulo-- c. Fun��es de agrega��o
-- i. Mostrar o t�tulo do CD e o t�tulo das m�sicas de todos os CD;
Select CD.cod_cd, Musica.cod_cd, CD.titulo, Musica.titulo
From CD, Musica
Where CD.cod_cd = Musica.cod_cd

-- ii. Mostrar o t�tulo do CD e o t�tulo da m�sica com o n�mero 1 de cada CD;
Select CD.cod_cd, Musica.cod_cd, CD.titulo as 'T�tulo CD', Musica.titulo as 'T�tulo M�sica', Musica.nr_musica
From CD, Musica
Where CD.cod_cd = Musica.cod_cd And Musica.nr_musica = 1

-- iii. Mostrar o n�mero, o t�tulo e a dura��o, de todas as m�sicas do CD com o t�tulo Punkzilla.
Select CD.cod_cd, Musica.cod_cd, Musica.nr_musica, Musica.titulo, Musica.duracao, CD.titulo
From CD, Musica
Where CD.cod_cd = Musica.cod_cd And CD.titulo Like 'Punkzilla'