-- Create Tables
Create Table Automovel (
			matricula Varchar(8) Constraint pk_Automovel_matricula Primary Key 
								 Constraint ck_Automovel_matricula Check (matricula Like '[a-z][a-z]-[0-9][0-9]-[0-9][0-9]' Or 
																		  matricula Like '[0-9][0-9]-[a-z][a-z]-[0-9][0-9]' Or 
																		  matricula Like '[0-9][0-9]-[0-9][0-9]-[a-z][a-z]'), 
			marca Varchar(30) Constraint nn_Automovel_marca Not Null, 
			cilindrada Int Constraint ck_Automovel_cilindrada Check (cilindrada Between 500 And 6000), 
			ano_fabrico Int Constraint ck_Automovel_ano_fabrico Check (ano_fabrico Between 1900 And Year(GetDate())), 
			preco_venda Decimal(9,2) Constraint ck_Automovel_preco_venda Check (preco_venda > 0)
)

Create Table Cliente (
			id_cliente Int Identity(1,1) Constraint pk_Cliente_id_cliente Primary Key 
										 Constraint ck_Cliente_id_cliente Check (id_cliente > 0), 
			nome Varchar(50) Constraint nn_Cliente_nome Not Null, 
			nr_identificacao_civil Int Constraint ck_Cliente_nr_identificacao_civil Check (nr_identificacao_civil Between 100000 And 999999999) 
									   Constraint uk_Cliente_nr_identificacao_civil Unique, 
			nif Int Constraint ck_Cliente_nif Check (nif > 0 And nif Between 100000000 And 999999999) 
				    Constraint uk_Cliente_nif Unique 
					Constraint nn_Cliente_nif Not Null, 
			data_nascimento Date Constraint nn_Cliente_data_nascimento Not Null
)

Create Table Revisao (
			matricula Varchar(8) Constraint fk_Revisao_matricula Foreign Key References Automovel(matricula), 
			data_hora_marcacao DateTime Default GetDate(), 
			efectuada Varchar(1) Constraint ck_Revisao_efectuada Check (efectuada Like '[SN]') Default 'N'
								 Constraint nn_Revisao_efectuada Not Null, 
			Constraint pk_Revisao_matricula_data_hora_marcacao Primary Key (matricula, data_hora_marcacao)
)

Create Table Automovel_Cliente (
			matricula Varchar(8) Constraint fk_Automovel_Cliente_matricula Foreign Key References Automovel(matricula), 
			id_cliente Int Constraint fk_Automovel_Cliente_id_cliente Foreign Key References Cliente(id_cliente), 
			Constraint pk_Automovel_Cliente_matricula_id_cliente Primary Key (matricula, id_cliente)
)

-- Insert Into Tables
Insert Into Automovel (matricula, marca, cilindrada, ano_fabrico, preco_venda) 
				Values ('45-PD-98', 'Mercedes', 2300, 2000, 34050), 
					   ('65-87-GR', 'Nissan', 1700, 2009, 23490.5), 
					   ('42-90-AS', 'Kia', 1300, 2008, 20870), 
					   ('BL-87-23', 'Volkswagen', 1100, 2017, 15600.75), 
					   ('83-QD-27', 'BMW', 2100, 2014, 35600), 
					   ('XO-65-98', 'Toyota', 2100, 2010, 15940)

Insert Into Cliente (nome, nr_identificacao_civil, nif, data_nascimento) 
				Values ('S�rgio Concei��o', 987345, 105098124, '1974-11-15'), 
					   ('Ant�nio Oliveira', 937587, 104052455, '1952-10-06'), 
					   ('Fernando Santos', Null, 102000906, '1954-10-10'), 
					   ('Artur Jorge', 7098428, 100829087, '1946-02-13'), 
					   ('Jesualdo Ferreira', 123456, 107559969, '1946-05-24')

Select * From Cliente

-- DBCC CHECKIDENT (Cliente, Reseed, 0)

Insert Into Automovel_Cliente (matricula, id_cliente) 
				Values ('65-87-GR', 1), 
					   ('83-QD-27', 4), 
					   ('42-90-AS', 2), 
					   ('45-PD-98', 1), 
					   ('XO-65-98', 5), 
					   ('BL-87-23', 3)

Insert Into Revisao (matricula, data_hora_marcacao, efectuada) 
				Values ('65-87-GR', '2018-10-04 09:00', 'N'), 
					   ('83-QD-27', '2018-11-04 14:45', 'N'), 
					   ('42-90-AS', '2018-10-23 10:50', 'N'), 
					   ('XO-65-98', '2018-12-01 18:30', 'N'), 
					   ('65-87-GR', '2018-06-07 10:50', 'S'), 
					   ('XO-65-98', '2016-11-22 12:20', 'S')

Select *
From Automovel

Select *
From Cliente

Select *
From Automovel_Cliente

Select *
From Revisao