-- Cast
Select p.FirstName, p.LastName, s.SalesYTD, s.BusinessEntityID
From Person.Person as p, Sales.SalesPerson as s
Where p.BusinessEntityID = s.BusinessEntityID And Cast(Cast(s.SalesYTD as Int) as Char(20)) Like '2%'

Select *
From Sales.SalesPerson

-- Convert
Select Substring(Name, 1, 30) as ProductName, ListPrice
From Production.Product
Where Convert(int, ListPrice) Like '3%'

-- Round
Select Round(123.9994, 3), Round(123.9995, 3)

-- SubString
Select SubString('diogo pinto', 1, 7)

Select name, SubString(name, 1, 1) as Initial, SubString(name, 3, 2) as ThirdAndFourthCharacters
From sys.databases
Where database_id < 5

-- Case
Select ProductNumber, ProductLine, Category =
	Case ProductLine
		When 'R' Then 'Road'
		When 'M' Then 'Mountain'
		When 'T' Then 'Touring'
		When 'S' Then 'Other Sale Items'
		Else 'Not For Sale'
	End, 
	Name
From Production.Product
Order By ProductNumber

Select ProductNumber, Name, ListPrice, 'Price Range' = 
	Case
		When ListPrice = 0 Then 'Mfg Item - Not For Resale'
		When ListPrice < 50 Then 'Under 50�'
		When ListPrice >= 50 and ListPrice < 250 Then 'Under 250�'
		When ListPrice >= 250 and ListPrice < 1000 Then 'Under 1000�'
	End
From Production.Product
Order By ProductNumber

-- Avg
Select Avg(ListPrice)
From Production.Product

Select Avg(Distinct ListPrice)
From Production.Product

-- Min
Select Min(TaxRate)
From Sales.SalesTaxRate

-- Max
Select Max(TaxRate)
From Sales.SalesTaxRate

-- Sum
Select Sum(TaxRate)
From Sales.SalesTaxRate

Select Color, Sum(ListPrice) as 'List Price Sum', Sum(StandardCost) as 'Standard Cost Sum'
From Production.Product
Where Color Is Not Null And ListPrice != 0.00 And Name Like 'Mountain%'
Group By Color
Order By Color

-- Count
Select Count(*)
From HumanResources.Employee

-- Grouping
Select SalesQuota, Sum(SalesYTD) as 'Total Sales YTD', Grouping(SalesQuota) as 'Grouping'
From Sales.SalesPerson
Group By SalesQuota With Rollup

-- Var
Select Var(Bonus)
From Sales.SalesPerson

-- Varp
Select Varp(Bonus)
From Sales.SalesPerson

-- Rank
Select i.ProductID, p.Name, i.LocationID, i.LocationID, i.Quantity,
	Rank() Over 
		(Partition By i.LocationID 
		 Order By i.Quantity Desc) as Rank
From Production.ProductInventory as i
Inner Join Production.Product as p On i.ProductID = p.ProductID
Where i.LocationID Between 3 and 4
Order By i.LocationID

-- Ntile
Select p.FirstName, p.LastName, Ntile(4) Over(Order By SalesYTD Desc) as 'Quartile', Convert(nvarchar(20), s.SalesYTD, 1) as 'SalesYTD', a.PostalCode
From Sales.SalesPerson as s
Inner Join Person.Person as p On s.BusinessEntityID = p.BusinessEntityID
Inner Join Person.Address as a On a.AddressID = p.BusinessEntityID
Where TerritoryID Is Not Null And SalesYTD <> 0

-- Dense_Rank
Select i.ProductID, p.Name, i.LocationID, i.LocationID, i.Quantity,
	Dense_Rank() Over 
		(Partition By i.LocationID 
		 Order By i.Quantity Desc) as Rank
From Production.ProductInventory as i
Inner Join Production.Product as p On i.ProductID = p.ProductID
Where i.LocationID Between 3 and 4
Order By i.LocationID

Select Top(10) BusinessEntityID, Rate, Dense_Rank() Over (Order By Rate Desc) as 'Rank by Salary'
From HumanResources.EmployeePayHistory

-- Row_Number
Select Row_Number() Over (Order By SalesYTD Desc) as 'Row', FirstName, LastName, Round(SalesYTD, 2, 1) as 'Sales YTD'
From Sales.vSalesPerson
Where TerritoryName Is Not Null And SalesYTD <> 0

-- All Ranking functions in the same query
Select p.FirstName, p.LastName, 
	Row_Number() Over (Order By a.PostalCode) as 'Row Number', 
	Rank() Over (Order by a.PostalCode) as 'Rank', 
	Dense_Rank() Over (Order By a.PostalCode) as 'Dense Rank', 
	Ntile(4) Over (Order By a.PostalCode) as 'Quartile', 
	s.SalesYTD, 
	a.PostalCode
From Sales.SalesPerson as s
Inner Join Person.Person as p On s.BusinessEntityID = p.BusinessEntityID
Inner Join Person.Address as a On a.AddressID = p.BusinessEntityID
Where TerritoryID Is Not Null And SalesYTD <> 0

-- Lead
Select TerritoryName, BusinessEntityID, SalesYTD, 
	Lead(SalesYTD, 1, 0) Over (Partition By TerritoryName Order By SalesYTD Desc) as 'NextRepSales'
From Sales.vSalesPerson
Where TerritoryName In(N'Northwest', N'Canada')
Order By TerritoryName

-- Lag
Select TerritoryName, BusinessEntityID, SalesYTD, 
	Lag(SalesYTD, 1, 0) Over (Partition By TerritoryName Order By SalesYTD Desc) as 'PrevRepSales'
From Sales.vSalesPerson
Where TerritoryName In(N'Northwest', N'Canada')
Order By TerritoryName

--Last_Value
Select Department, LastName, Rate, HireDate, 
	Last_Value(HireDate) Over (Partition By Department Order By Rate) as 'LastValue'
From HumanResources.vEmployeeDepartmentHistory as edh
Inner Join HumanResources.EmployeePayHistory as eph On eph.BusinessEntityID = edh.BusinessEntityID
Inner Join HumanResources.Employee as e On e.BusinessEntityID = edh.BusinessEntityID
Where Department In(N'Information Services', N'Document Control')

-- First_Value
Select Name, ListPrice, 
	First_Value(Name) Over (Order By ListPrice Asc) As 'LeastExpensive'
From Production.Product
Where ProductSubcategoryID = 37

-- Percent_Rank
Select Department, LastName, Rate, 
	Cume_Dist() Over (Partition By Department Order By Rate) as 'CumeDist', 
	Percent_Rank() Over (Partition By Department Order By Rate) as 'PercentRank'
From HumanResources.vEmployeeDepartmentHistory as edh
Inner Join HumanResources.EmployeePayHistory as e On e.BusinessEntityID = edh.BusinessEntityID
Where Department In(N'Information Services', N'Document Control')
Order By Department, Rate Desc

-- Cume_Dist
Select Department, LastName, Rate, 
	Cume_Dist() Over (Partition By Department Order By Rate) as 'CumeDist', 
	Percent_Rank() Over (Partition By Department Order By Rate) as 'PercentRank'
From HumanResources.vEmployeeDepartmentHistory as edh
Inner Join HumanResources.EmployeePayHistory as e On e.BusinessEntityID = edh.BusinessEntityID
Where Department In(N'Information Services', N'Document Control')
Order By Department, Rate Desc

-- Percentile_Disc
Select Distinct Name as 'Department Name', 
	Percentile_Cont(0.5) Within Group (Order By ph.Rate) Over (Partition By Name) as 'MedianCont', 
	Percentile_Disc(0.5) Within Group (Order By ph.Rate) Over (Partition By Name) as 'MedianDisc'
From HumanResources.Department as d
Inner Join HumanResources.EmployeeDepartmentHistory as dh On dh.DepartmentID = d.DepartmentID
Inner Join HumanResources.EmployeePayHistory as ph On ph.BusinessEntityID = dh.BusinessEntityID
Where dh.EndDate Is Null

-- Percentile_Cont
Select Distinct Name as 'Department Name', 
	Percentile_Cont(0.5) Within Group (Order By ph.Rate) Over (Partition By Name) as 'MedianCont', 
	Percentile_Disc(0.5) Within Group (Order By ph.Rate) Over (Partition By Name) as 'MedianDisc'
From HumanResources.Department as d
Inner Join HumanResources.EmployeeDepartmentHistory as dh On dh.DepartmentID = d.DepartmentID
Inner Join HumanResources.EmployeePayHistory as ph On ph.BusinessEntityID = dh.BusinessEntityID
Where dh.EndDate Is Null

-- Except
Select ProductID
From Production.Product

Select ProductID
From Production.Product
Except
Select ProductID
From Production.WorkOrder

-- Intersect
Select ProductID
From Production.Product
Intersect
Select ProductID
From Production.WorkOrder

-- Union
If OBJECT_ID('dbo.Gloves', 'U') Is Not Null
Drop Table dbo.Gloves
Go
-- Create Gloves Table
Select ProductModelID, Name
Into dbo.Gloves
From Production.ProductModel
Where ProductModelID In(3, 4)
Go
-- The Union
Select ProductModelID, Name
From Production.ProductModel
Where ProductModelID Not In(3, 4)
Union
Select ProductModelID, Name
From dbo.Gloves
Order By Name

-- Stored Procedures
Go
Create Procedure HumanResources.uspGetEmployeesTest2
	@LastName nvarchar(50), 
	@FirstName nvarchar(50)
As
	Set Nocount On
	Select FirstName, LastName, Department
	From HumanResources.vEmployeeDepartmentHistory
	Where FirstName = @FirstName And LastName = @LastName And EndDate Is Null
Go

Execute HumanResources.uspGetEmployeesTest2 N'Ackerman', N'Pilar'
-- Or
Exec HumanResources.uspGetEmployeesTest2 @LastName = N'Ackerman', @FirstName = N'Pilar'
Go
-- Or
Execute HumanResources.uspGetEmployeesTest2 @FirstName = N'Pilar', @LastName = N'Ackerman'
Go

-- Modify Stored Procedure
GO
ALTER Procedure [HumanResources].[uspGetEmployeesTest2]
	@LastName nvarchar(60), 
	@FirstName nvarchar(60)
As
	Set Nocount On
	Select FirstName, LastName, Department
	From HumanResources.vEmployeeDepartmentHistory
	Where FirstName = @FirstName And LastName = @LastName And EndDate Is Null
Go

-- Delete Stored Procedure
Drop Procedure HumanResources.uspGetEmployeesTest2
Go